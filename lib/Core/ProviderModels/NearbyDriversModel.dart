import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:WeRide/Core/Models/Drivers.dart';
import 'package:WeRide/Core/Repository/Repository.dart';

class NearbyDriversModel extends ChangeNotifier {
  List<Driver> nearbyDrivers;
  final nearbyDriverStreamController = StreamController<List<Driver>>();

  get nearbyDriverList => nearbyDrivers;

  Stream<List<Driver>> get dataStream => nearbyDriverStreamController.stream;

  NearbyDriversModel() {
    //We will be listening to the nearbyDrivers events like, there location Updates etc.
    
    Repository.getNearbyDrivers(nearbyDriverStreamController);

    dataStream.listen((list) {
      nearbyDrivers = list;
      notifyListeners();
    });
  }

  void closeStream() {
    nearbyDriverStreamController.close();
  }
}
