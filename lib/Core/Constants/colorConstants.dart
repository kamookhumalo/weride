import 'package:flutter/cupertino.dart';

class ConstantColors {
  static const Color PrimaryColor = Color(0xff656d74);
  static const Color ActivePink = Color(0xFF000000);
  static const Color DeepBlue = Color(0xFF9E9D24);
}
